#pragma once
#include "DEFINITIONS.h"
#include "BOUNDINGBOX.h"
#include "PLAYER.h"
#include "ENEMY.h"
#include "ENEMY2.h"
#include "ENEMY3.h"
#include "ENEMY4.h"
#include "INPUT_MANAGER.h"

#include<allegro5\allegro.h>
#include<allegro5\allegro_primitives.h>
#include<allegro5\allegro_image.h>


class PLATFORM
{
public:
	PLATFORM();
	PLATFORM(float posX, float posY, int type);
	~PLATFORM();
	virtual void Update(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *secTimer, ALLEGRO_TIMER *animationTimer, std::vector<PROJECTILE*> &playerProjectiles);
	virtual void Draw();

	float getPosX();
	float getPosY();
	float getWidth();
	float getHeight();
	BOUNDINGBOX *bbox;

	bool onPlatform;

	virtual void Collision(PLAYER *player);
protected:
	float posX;
	float posY;
	int width;
	int height;
	int type;

	ENEMY *enemy;
	ENEMY2 *enemy2;
	ENEMY3 *enemy3;
	ENEMY4 *enemy4;

	ALLEGRO_BITMAP *image;

	void SpawnEnemy(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER *animationTimer, std::vector<PROJECTILE*> &playerProjectiles);
};