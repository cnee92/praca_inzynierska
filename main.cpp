#include <allegro5\allegro.h>
#include <allegro5\allegro_native_dialog.h>
#include <allegro5\allegro_primitives.h>

#include "DEFINITIONS.h"
#include "GAME.h"




int main()
{

	srand(time(NULL));

	////////////////////////////////////////////////////////////////////////////////////////
	if (!al_init())
		al_show_native_message_box(NULL, NULL, NULL, "initialize allegro 5", NULL, NULL);
	
	al_install_keyboard();

	al_init_image_addon();
	al_init_primitives_addon();
	al_init_font_addon();
	al_init_ttf_addon();

	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();
	ALLEGRO_TIMER *timer = al_create_timer(1.0 / FPS);
	ALLEGRO_TIMER *animationTimer = al_create_timer(1.0 / ANIMATION_SPEED);
	ALLEGRO_TIMER *timer6fps = al_create_timer(1.0 / 6);
	ALLEGRO_TIMER *timer1fps = al_create_timer(1.0);

	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_timer_event_source(animationTimer));
	al_register_event_source(event_queue, al_get_timer_event_source(timer6fps));
	al_register_event_source(event_queue, al_get_timer_event_source(timer1fps));
	
	//ALLEGRO_EVENT ev;
	//ALLEGRO_TIMER *timer = al_create_timer(1.0 / FPS);
	//ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();
	//al_register_event_source(event_queue, al_get_timer_event_source(timer));
	//al_start_timer(timer);


	////////////////////////////////////////////////////////////////////////////////////////

	bool done = false;

	GAME *game = new GAME(event_queue, timer, animationTimer, timer6fps, timer1fps);
	
	al_start_timer(timer);
	al_start_timer(animationTimer);
	al_start_timer(timer6fps);
	al_start_timer(timer1fps);

	while (!game->getDone())
	{
		game->Update();
		game->Draw();
	}
	
	al_destroy_timer(timer);
	al_destroy_timer(animationTimer);
	al_destroy_timer(timer6fps);
	
	////////////////////////////////////////////////////////////////////////////////////////

	return 0;
}
