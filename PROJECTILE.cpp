#include "PROJECTILE.h"

PROJECTILE::PROJECTILE()
{}

PROJECTILE::PROJECTILE(PLAYER* player)
{
	if(!player->getIsDDActive())
		image = al_load_bitmap("Img/blueProjectile.png");
	else
		image = al_load_bitmap("Img/greenProjectile.png");

	this->dirLeft = player->projectileDirLeft;
	this->dirRight = player->projectileDirRight;

	if(dirRight)
		this->posX = player->posX + 5 + player->getWidth() / 1.5;

	if(dirLeft)
		this->posX = player->posX + 10;

	this->posY = player->projectilePosY+1;
	this->width = al_get_bitmap_width(image);
	this->height = al_get_bitmap_height(image);

	this->startPosX = player->posX;

	this->dmg = player->getDmg();
	range = 600;
	moveSpeed = 13;

	isActive = isAlive = true;
	Explosion = false;
}


PROJECTILE::~PROJECTILE()
{
	delete bbox;
	al_destroy_bitmap(image);
}

void PROJECTILE::Update(ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer)
{
	if (isAlive)
	{
		delete bbox;
		bbox = new BOUNDINGBOX(posX, posY, width, height);
		Move(ev, timer);
	}
}

void PROJECTILE::Draw(PLAYER *player)
{
	if (isAlive)
		al_draw_bitmap(image, posX, posY, NULL);
	else
		if(!player->getIsDDActive())
			StartExplosion("Img/blueprojectileExplo.png");
		else
			StartExplosion("Img/greenprojectileExplo.png");
}

void PROJECTILE::Move(ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer)
{
	if (ev.type == ALLEGRO_EVENT_TIMER)
		if (ev.timer.source == timer)
		{
			if (dirRight)
				posX += moveSpeed;
			if (dirLeft)
				posX -= moveSpeed;
		}
}

void PROJECTILE::StartExplosion(const char* imageDir)
{
	if (!isAlive && !Explosion)
	{
		image = al_load_bitmap(imageDir);
		width = 66.6669;
		height = al_get_bitmap_height(image);
		ExplosionImgSourceX = 0;
		Explosion = true;
	}

	al_draw_bitmap_region(image, ExplosionImgSourceX, 0, width, height, posX, posY - height / 2, NULL);
	ExplosionImgSourceX += width;
	if (ExplosionImgSourceX >= al_get_bitmap_width(image))
		isActive = false;
}


//###################################################################
float PROJECTILE::getPosX()
{
	return posX;
}
float PROJECTILE::getStartPosX()
{
	return startPosX;
}
int PROJECTILE::getWidth()
{
	return width;
}
int PROJECTILE::getHeight()
{
	return height;
}
int PROJECTILE::getRange()
{
	return range;
}
bool PROJECTILE::getIsActive()
{
	return isActive;
}
void PROJECTILE::setIsActive(bool isActive)
{
	this->isActive = isActive;
}
bool PROJECTILE::getIsAlive()
{
	return isAlive;
}
void PROJECTILE::setIsAlive(bool isAlive)
{
	this->isAlive = isAlive;
}

