#include "BACKGROUND.h"
#include <iostream>



BACKGROUND::BACKGROUND(ALLEGRO_DISPLAY *display)
{
	CreateBackgroundImage(display);
	backgroundImage = al_load_bitmap("Img/background.png");
	this->posY = 0;
}


BACKGROUND::~BACKGROUND()
{
}

void BACKGROUND::Update(float posX)
{
	this->posX = posX/10;
}

void BACKGROUND::Draw(float cameraPosX)
{
	al_draw_bitmap_region(backgroundImage, posX, posY, SCREEN_WIDTH, SCREEN_HEIGHT, cameraPosX, posY, NULL);
	
	al_draw_bitmap_region(bridgeImage, cameraPosX, posY, SCREEN_WIDTH, SCREEN_HEIGHT, cameraPosX, SCREEN_HEIGHT - al_get_bitmap_height(bridgeImage), NULL);
}

void BACKGROUND::CreateBackgroundImage(ALLEGRO_DISPLAY *display)
{
	sourceBridgeImage = al_load_bitmap("Img/bridge.png");
	bridgeImage = al_create_bitmap(15000, al_get_bitmap_height(sourceBridgeImage));

	al_set_target_bitmap(bridgeImage);

	for (int i = 0; i < 15000/al_get_bitmap_width(sourceBridgeImage); i++)
		al_draw_bitmap(sourceBridgeImage, i*SCREEN_WIDTH, 0, NULL);
	al_set_target_bitmap(al_get_backbuffer(display));

	al_destroy_bitmap(sourceBridgeImage);
}