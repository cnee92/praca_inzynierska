#pragma once

#include <iostream>

#include<allegro5\allegro.h>
#include<allegro5\allegro_native_dialog.h>
#include<allegro5\allegro_primitives.h>
#include<fstream>
#include<string>
#include<algorithm>


#include "DEFINITIONS.h"
#include "PLAYER.h"
#include "PLATFORM.h"
#include "CAMERA.h"
#include "INPUT_MANAGER.h"
#include "BACKGROUND.h"
#include "ENEMY.h"
#include "ENEMY2.h"
#include "ENEMY3.h"
#include "ENEMY4.h"
#include "SPINNER_ENEMY.h"
#include "POWERUP.h"
#include "BOSS.h"
#include "OBSTACKLE.h"


class GAME
{
public:
	GAME(ALLEGRO_EVENT_QUEUE *event_queue, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *animationTimer, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER *timer1fps);
	~GAME();
	
	bool getDone();
	void Update();
	void Draw();
private:	
	ALLEGRO_DISPLAY *display;
	ALLEGRO_EVENT_QUEUE *event_queue;
	ALLEGRO_EVENT ev;
	ALLEGRO_TIMER *timer;
	ALLEGRO_TIMER *animationTimer;
	ALLEGRO_TIMER *timer6fps;
	ALLEGRO_TIMER *timer1fps;
	ALLEGRO_BITMAP* gameOverImg;
	ALLEGRO_FONT *font;


	PLAYER *player;
	vector<PROJECTILE*>playerProjectiles;

	CAMERA camera;
	INPUT_MANAGER input;
	BACKGROUND *background;

	
	vector<ENEMY*> enemies;
	ENEMY* groundEnemies[10] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

	vector<SPINNER_ENEMY*> spinnerEnemies;

	SPINNER_ENEMY *spinner;
	vector<PLATFORM*> platforms;
	vector<OBSTACKLE*> obstackles;

	POWERUP powerup;

	BOSS *boss;

	bool gameOver;
	bool bossDied;
	bool bossIsActive;

	bool done;
	int alpha;


	void SpawnEnemy();
	void LoadMap();
	void PlayerShoot();
	void GameOver();
	void SpawnBoss();
	void GameOverDraw();
};

