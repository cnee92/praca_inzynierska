#pragma once
#include "ENEMY.h"
class ENEMY3 :
	public ENEMY
{
public:
	ENEMY3(float posX, float posY, bool isOnPlatform);
	~ENEMY3();
private:
	void SpawnProjectile(ALLEGRO_EVENT ev, PLAYER* player, ALLEGRO_TIMER *secTimer, ALLEGRO_TIMER* timer);

};

