#pragma once
#include <allegro5\allegro.h>
#include <allegro5\allegro_image.h>
#include <iostream>
#include "BOUNDINGBOX.h"
#include "PLAYER.h"



class PROJECTILE
{
public:
	PROJECTILE();
	PROJECTILE(PLAYER *player);
	~PROJECTILE();

	virtual void Update(ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer);
	virtual void Draw(PLAYER *player);

	virtual float getPosX();
	virtual float getStartPosX();
	virtual int getWidth();
	virtual int getHeight();
	virtual int getRange();

	virtual bool getIsActive();
	virtual void setIsActive(bool isActive);

	virtual bool getIsAlive();
	virtual void setIsAlive(bool isAlive);

	BOUNDINGBOX *bbox;
	int dmg;

protected:
	float posX;
	float posY;
	float width;
	float height;
	int range;
	
	float moveSpeed;

	float startPosX;

	int dirRight;
	int dirLeft;

	bool isActive;
	bool isAlive;
	bool Explosion;

	float ExplosionImgSourceX;
	float ExplosionImgSourceY;

	ALLEGRO_BITMAP *image;
	virtual void Move(ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer);
	virtual void StartExplosion(const char* imageDir);
};







