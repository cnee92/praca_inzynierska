#include "ENEMY.h"
#include <iostream>


ENEMY::ENEMY()
{}

ENEMY::ENEMY(float posX, float posY, bool isOnPlatform)
{
	float imageFrameX = 6;
	float imageFrameY = 2;

	image = al_load_bitmap("Img/Cov1.png");
	this->width = al_get_bitmap_width(image) / imageFrameX;
	this->height = al_get_bitmap_height(image) / imageFrameY;

	this->posX = this->prevPosX = this->startPosX  = (rand() % 280) + posX;
	this->posY = posY- height;

	this->bboxSizeX = 40;
	this->bboxSizeY = 0;

	this->exp = 25;
	this->hpBorder = 100;
	this->maxHp = 150;
	this->currentHp = maxHp;
	this->hpBar = 100;


	bbox = new BOUNDINGBOX(posX+bboxSizeX, posY + bboxSizeY, width - bboxSizeX, height - bboxSizeY);
	prevBbox = new BOUNDINGBOX(posX + bboxSizeX, posY + bboxSizeY, width - bboxSizeX, height - bboxSizeY);

	this->projectleTime = 0;
	moveSpeed = 3;
	isActive = isAlive = true;
	deathExplosion = false;
	
	this->isOnPlatform = isOnPlatform;
	this->limitPosX = posX+10;

	if ((rand() % 10) < 5)
		onPlatformDir = false;
	else
		onPlatformDir = true;
}


ENEMY::~ENEMY()
{
	al_destroy_bitmap(image);
	delete bbox;
}

void ENEMY::Update(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER *animationTimer, std::vector<PROJECTILE*> &playerProjectiles)
{
	if (currentHp <= 0)
	isAlive = false;

	float distance = posX - player->posX;

	if (isAlive)
	{
		Move(distance, ev, timer, animationTimer);
		delete bbox;
		delete prevBbox;
		bbox = new BOUNDINGBOX(posX + bboxSizeX, posY + bboxSizeY, width - 2*bboxSizeX, height - bboxSizeY);
		prevBbox = new BOUNDINGBOX(prevPosX + bboxSizeX, prevPosY + bboxSizeY, width - 2 * bboxSizeX, height - bboxSizeY);
		Collision(playerProjectiles);
		SpawnProjectile(ev, player, timer6fps, timer);
	}

	if (!isAlive && !deathExplosion)
	{
		player->addExperience(this->exp);
		image = al_load_bitmap("Img/dead.png");
		width = al_get_bitmap_width(image) / 9;
		newHeight = al_get_bitmap_height(image);
		imageSourceX = 0;
		imageSourceY = 0;
		deathExplosion = true;
	}

	if (ev.type == ALLEGRO_EVENT_TIMER)
		if (ev.timer.source == animationTimer)
		{
			if (!isAlive)
				imageSourceX += width;
			if (imageSourceX >= al_get_bitmap_width(image))
				isActive = false;
		}
}

void ENEMY::Draw()
{
//bbox->Draw();
	if (isAlive)
	{
		if(currentHp>=0)
		DrawHpBar();

		if (projectiles.size() > 0)
			for (int i = 0; i < projectiles.size(); i++)
				projectiles[i]->Draw();

		al_draw_bitmap_region(image, imageSourceX, imageSourceY * height, width, height, posX, posY, NULL);
	}
	else
		al_draw_bitmap_region(image, imageSourceX, imageSourceY, width, newHeight, posX+10, posY+(height - newHeight), NULL);
}

void ENEMY::DrawHpBar()
{
	hpBar = hpBorder * (currentHp / maxHp);

	float hpBarPosX = (posX + width / 2) - (hpBorder / 2);
	al_draw_filled_rectangle(hpBarPosX, posY - 20, hpBarPosX + hpBar, posY - 10, al_map_rgb(255, 0, 0));
	al_draw_rectangle(hpBarPosX, posY - 20, hpBarPosX + hpBorder, posY - 10, al_map_rgb(0, 0, 0), 3);
}

void ENEMY::Move(float distance, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *animationTimer)
{
	if (ev.type == ALLEGRO_EVENT_TIMER)
		if (ev.timer.source == timer)
		{
			if (posX != prevPosX)
				animationActive = true;
			else
				animationActive = false;

			prevPosX = posX;

			if (!isOnPlatform)
			{
				if (distance<=800)
				{
				if (distance > 255)
						posX -= moveSpeed;
				else if (distance < 250 && distance > 0)
						posX += moveSpeed;
				}

				if (distance>-800)
				{
					if (distance < -255)
						posX += moveSpeed;
					else if (distance > -250 && distance < 0)
						posX -= moveSpeed;
				}

				if (distance > 0)
					imageSourceY = 0;
				else
					imageSourceY = 1;
			}
			else
			{
				if (posX <= limitPosX)
				{
					onPlatformDir = true;
					imageSourceY = 1;
				}
				else if (posX >= limitPosX + 300 - width)
				{
					onPlatformDir = false;
					imageSourceY = 0;
				}

				if (onPlatformDir)
					posX += moveSpeed;
				else
					posX -= moveSpeed;
			}
		}

		//ANIMACJA
		if (ev.timer.source == animationTimer)
		{
			if (animationActive)
				imageSourceX += width;
			else
				imageSourceX = 0;

			if (imageSourceX >= al_get_bitmap_width(image)-10)
				imageSourceX = 0;
		}
}

void ENEMY::Collision(std::vector<PROJECTILE*> &playerProjectiles)
{
	for (int i = 0; i < playerProjectiles.size(); i++)
		if (playerProjectiles[i]->getIsAlive())
			if (bbox->Intersects(playerProjectiles[i]->bbox))
			{
			currentHp -= playerProjectiles[i]->dmg;
 			playerProjectiles[i]->setIsAlive(false);
			}
}


void ENEMY::SpawnProjectile(ALLEGRO_EVENT ev, PLAYER* player, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER* timer)
{
	if (ev.type == ALLEGRO_EVENT_TIMER)
		if (ev.timer.source == timer6fps)
		{
			if (!isOnPlatform)
			{
				if (projectleTime < 3)
					projectiles.push_back(new ENEMY_PROJECTILE(player->posX, posX, posY, width, height));
			}
			else
				if (projectleTime < 3)
					projectiles.push_back(new ENEMY_PROJECTILE(onPlatformDir, posX, posY, width, height));

			projectleTime++;

			if (projectleTime >= 7)
				projectleTime = 0;
		}


	for (int i = 0; i < projectiles.size(); i++)
	{
		projectiles[i]->Update(player, ev, timer);

		if (projectiles[i]->getPosX() > projectiles[i]->getStartPosX() + SCREEN_WIDTH / 1.5)
			projectiles[i]->setIsActive(false);
		else if (projectiles[i]->getPosX() < projectiles[i]->getStartPosX() - SCREEN_WIDTH / 1.5)
			projectiles[i]->setIsActive(false);

		if (!projectiles[i]->getIsActive())
		{
			delete projectiles[i];
			projectiles.erase(projectiles.begin() + i);
		}
	}
}


//########################################

bool ENEMY::getIsActive()
{
	return isActive;
}
void ENEMY::setIsActive(bool isActive)
{
	this->isActive = isActive;
}
int ENEMY::getExp()
{
	return exp;
}

vector<ENEMY_PROJECTILE*> ENEMY::getEnemiesProjectiles()
{
	return projectiles;
}
