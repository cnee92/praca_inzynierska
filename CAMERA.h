#pragma once
#include <allegro5\/allegro.h>
#include "PLAYER.h"

class CAMERA
{
public:
	CAMERA();
	~CAMERA();

	void Update(float x, float y, float width, float height);
	float* getCameraPos();
private:
	
	float cameraPos[2];
	ALLEGRO_TRANSFORM camera;
};

