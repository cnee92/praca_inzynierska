#include "CAMERA.h"

#include <iostream>


CAMERA::CAMERA()
{
}


CAMERA::~CAMERA()
{
}

void CAMERA::Update(float x, float y, float width, float height)
{
	cameraPos[0] = -(SCREEN_WIDTH / 2) + (x + width / 2);
	cameraPos[1] = -(SCREEN_HEIGHT / 2);

	if (cameraPos[0] < 0)
		cameraPos[0] = 0;
	else if (cameraPos[0] > 12700)
		cameraPos[0] = 12700;
	if (cameraPos[1] < 0)
		cameraPos[1] = 0;


	al_identity_transform(&camera);
	al_translate_transform(&camera, -cameraPos[0], -cameraPos[1]);
	al_use_transform(&camera);
}

float* CAMERA::getCameraPos()
{
	return cameraPos;
}