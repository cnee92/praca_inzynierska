#include "BOUNDINGBOX.h"
#include <iostream>

BOUNDINGBOX::~BOUNDINGBOX()
{
}

BOUNDINGBOX::BOUNDINGBOX(float x, float y, float w, float h)
	: left(x), right(x + w), top(y), bottom(y + h)
{
}

bool BOUNDINGBOX::Intersects(BOUNDINGBOX *obj)
{
	if (right <  obj->left || left > obj->right || top > obj->bottom || bottom < obj->top)
		return false;

	return true;
}

void BOUNDINGBOX::Draw()
{
	al_draw_rectangle(left, top, right, bottom, al_map_rgb(0, 255, 0), 3);
}