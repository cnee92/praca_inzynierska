#include "BOSS.h"
#include <iostream>

BOSS::BOSS()
{
	isActive = true;

	eyeBBox = new BOUNDINGBOX(headPosX + 140, headPosY + 110, 80, 110);
	

	hpBorderImage = al_load_bitmap("Img/hpBorder.png");
	font = al_load_font("Fonts/batmfa.ttf", 20, NULL);
	maxHp = 3500;
	currentHp = maxHp;
	hpBorderSize = 600;
	hpBar = 600;
	hpBarPosX = 350;
	hpBarPosY = SCREEN_HEIGHT - 100;

	opacity = 0;

	headImage = al_load_bitmap("Img/boos12.png");
	headPosX = 13000+450;
	headPosY = -50;
	headStartPosX = headPosX;
	headStartPosY = headPosY;
	headActive = false;
	headFlag = true;


	jawImage = al_load_bitmap("Img/szczena.png");
	jawPosX = headPosX + 227 + al_get_bitmap_width(jawImage) + 10;
	jawPosY = headPosY + 150 - 15;
	jawDegree = 0;
	jawActive = false;
	jawDown = true;
	jawUp = false;



	bigAttackBBox = NULL;
	bigAttackImage = al_load_bitmap("Img/startBigAttack.png");
	fullBigAttackImage = al_load_bitmap("Img/fullBigAttack.png");
	bigAttackWidth = al_get_bitmap_width(bigAttackImage);
	bigAttackHeight = al_get_bitmap_height(bigAttackImage);
	bigAttackPosX = headPosX + 150;
	bigAttackPosY = headPosY + 400;
	bigAttackScale = 0.0f;
	bigAttackActive = false;

	fullBigAttackWidth = al_get_bitmap_width(fullBigAttackImage);
	fullBigAttackHeight = al_get_bitmap_height(fullBigAttackImage);
	

	smallAttackBBox = NULL;
	smallAttackImage = al_load_bitmap("Img/startSmallAttack.png");
	fullSmallAttackImage = al_load_bitmap("Img/fullSmallAttack.png");
	smallAttackWidth = al_get_bitmap_width(smallAttackImage);
	smallAttackHeight = al_get_bitmap_height(smallAttackImage);
	smallAttackPosX = headPosX + 150;
	smallAttackPosY = headPosY + 100;
	smallAttackScale = 0.9f;
	smallAttackActive = false;

	fullSmallAttackWidth = al_get_bitmap_width(fullSmallAttackImage);
	fullSmallAttackHeight = al_get_bitmap_height(fullSmallAttackImage);
}


BOSS::~BOSS()
{
}

void BOSS::Update(ALLEGRO_EVENT ev, ALLEGRO_TIMER *animationTimer, PLAYER *player, std::vector<PROJECTILE*> &playerProjectiles, bool &bossDied, bool &bossIsActive)
{	
	if (currentHp>=0)
	hpText = to_string((int)currentHp) + " / " + to_string((int)maxHp);

	if (currentHp > 0)
	{
		JawMove(ev, animationTimer, player);
		SetAttackPositions();

		Collision(player, playerProjectiles);

		HeadMove(ev, animationTimer);
	}

	if (currentHp <= 0 && headPosX < 14000)
	{
		jawPosX++;
		headPosX++;
		headStartPosX++;
		bossIsActive = false;
		currentHp = 0;
	}
	else if (headPosX >= 14000)
		bossDied = true;
}

void BOSS::Draw(float cameraPosX)
{
	
	al_draw_bitmap(headImage, headPosX, headPosY, NULL);
	al_draw_rotated_bitmap(jawImage, al_get_bitmap_width(jawImage), 0, jawPosX, jawPosY, jawDegree * 3.14159 / 180, NULL);
	
	if(currentHp>0)
	DrawAttacks();

	DrawHpBar(cameraPosX);

	//eyeBBox->Draw();

}

void BOSS::JawMove(ALLEGRO_EVENT ev, ALLEGRO_TIMER *animationTimer, PLAYER* player)
{

if (ev.type == ALLEGRO_EVENT_TIMER)
	if (ev.timer.source == animationTimer)
	{
		if (rand() % 25 == 5 && !jawActive)
			jawActive = true;

		if (jawActive)
		{
			if (jawDown)
			{
				jawDegree -= 0.375;
				bigAttackScale += 0.0225;
				smallAttackScale -= 0.0225;
			}
			else if (jawUp)
			{
				jawDegree += 0.375;
				bigAttackScale -= 0.0225;
				smallAttackScale += 0.0225;
				bigAttackActive = false;
			}
	

			if (jawDegree >= 0)
			{
				jawActive = false;
				jawUp = false;
				jawDown = true;
			}
			else if (jawDegree <= -15)
			{
				jawActive = false;
				jawUp = true;
				jawDown = false;
				bigAttackActive = true;
				smallAttackActive = true;
			}
		}
	}
}

void BOSS::HeadMove(ALLEGRO_EVENT ev, ALLEGRO_TIMER *animationTimer)
{
	if (ev.type == ALLEGRO_EVENT_TIMER)
		if (ev.timer.source == animationTimer)
		{
			if (rand() % 25 == 5 && !headActive)
			{
				headActive = true;
				headFlag = true;

				float random = ((rand() % 10) + 1);
				headMoveX = 1.5f - (1.0f / ((rand() % 10) + 1));
				headMoveY = 1.0f - (1.0f / ((rand() % 10) + 1));
			}

			if (headActive)
			{
				if (headStartPosX - headPosX < 30 && headFlag)
				{
					headPosX -= headMoveX;
					headPosY += headMoveY;
					jawPosX -= headMoveX;
					jawPosY += headMoveY;
				}
				else if (headStartPosX - headPosX > 0)
				{
					headFlag = false;
					headPosX += headMoveX;
					headPosY -= headMoveY;
					jawPosX += headMoveX;
					jawPosY -= headMoveY;
				}
				else if (headStartPosX - headPosX == 0)
					headActive = false;
			}
		}

}

void BOSS::Collision(PLAYER *player, std::vector<PROJECTILE*> &playerProjectiles)
{

	if (jawUp)
	{
		delete bigAttackBBox;
		bigAttackBBox = new BOUNDINGBOX(fullBigAttackPosX, fullBigAttackPosY, fullBigAttackWidth, fullBigAttackHeight*bigAttackScale);
		if (bigAttackBBox->Intersects(player->bbox))
			player->subCurrentHp(1);
	}
	else
	{
		delete bigAttackBBox;
		bigAttackBBox = new BOUNDINGBOX(bigAttackPosX, bigAttackPosY, bigAttackWidth, bigAttackHeight*bigAttackScale);
		if (bigAttackBBox->Intersects(player->bbox))
			player->subCurrentHp(1);
	}

	if (jawDown)
	{
		delete smallAttackBBox;
		smallAttackBBox = new BOUNDINGBOX(fullSmallAttackPosX + 280, fullSmallAttackPosY, fullSmallAttackWidth, fullSmallAttackHeight*smallAttackScale);
		if (smallAttackBBox->Intersects(player->bbox))
			player->subCurrentHp(1);
	}

	delete eyeBBox;
	eyeBBox = new BOUNDINGBOX(headPosX + 160, headPosY + 110, 60, 110);
	for (int i = 0; i < playerProjectiles.size(); i++)
		if (eyeBBox->Intersects(playerProjectiles[i]->bbox))
		{
			if (playerProjectiles[i]->getIsAlive())
				currentHp -= playerProjectiles[i]->dmg;
			playerProjectiles[i]->setIsAlive(false);
		}
}

void BOSS::SetAttackPositions()
{
	bigAttackPosX = headPosX - 60 + (bigAttackWidth / 2.0f - (bigAttackWidth*(bigAttackScale / 2.0f)));
	bigAttackPosY = headPosY + 350.0f + (bigAttackHeight / 2.0f - (bigAttackHeight*(bigAttackScale / 2.0f)));
	fullBigAttackPosX = bigAttackPosX - 1420;
	fullBigAttackPosY = bigAttackPosY;

	smallAttackPosX = headPosX - 40 + (smallAttackWidth / 2.0f - (smallAttackWidth*(smallAttackScale / 2.0f)));
	smallAttackPosY = headPosY + 150 + (smallAttackHeight / 2.0f - (smallAttackHeight*(smallAttackScale / 2.0f)));
	fullSmallAttackPosX = smallAttackPosX - 1110;
	fullSmallAttackPosY = smallAttackPosY;
}

void BOSS::DrawHpBar(float posX)
{
	hpBar = hpBorderSize * (currentHp / maxHp);
	if (currentHp>0)
		al_draw_filled_rectangle(hpBarPosX + posX, hpBarPosY, hpBarPosX + posX + hpBar, SCREEN_HEIGHT - 70 + 7, al_map_rgb(255, 0, 0));

	al_draw_bitmap(hpBorderImage, hpBarPosX + posX, hpBarPosY, NULL);
	al_draw_text(font, al_map_rgb(255, 255, 255), hpBarPosX + posX + al_get_bitmap_width(hpBorderImage) / 2 - al_get_text_width(font, hpText.c_str()) / 2, SCREEN_HEIGHT - 90, NULL, hpText.c_str());
}

void BOSS::DrawAttacks()
{
	al_draw_scaled_bitmap(bigAttackImage, 0, 0, bigAttackWidth, bigAttackHeight, bigAttackPosX, bigAttackPosY, bigAttackWidth*bigAttackScale, bigAttackHeight*bigAttackScale, NULL);

	if (jawUp)
		al_draw_scaled_bitmap(fullBigAttackImage, 0, 0, al_get_bitmap_width(fullBigAttackImage), al_get_bitmap_height(fullBigAttackImage), fullBigAttackPosX, fullBigAttackPosY, al_get_bitmap_width(fullBigAttackImage), al_get_bitmap_height(fullBigAttackImage)*bigAttackScale, NULL);
	else
		al_draw_scaled_bitmap(bigAttackImage, 0, 0, bigAttackWidth, bigAttackHeight, bigAttackPosX, bigAttackPosY, bigAttackWidth*bigAttackScale, bigAttackHeight*bigAttackScale, NULL);


	al_draw_scaled_bitmap(smallAttackImage, 0, 0, smallAttackWidth, smallAttackHeight, smallAttackPosX, smallAttackPosY, smallAttackWidth*smallAttackScale, smallAttackHeight*smallAttackScale, NULL);

	if (jawDown)
		al_draw_scaled_bitmap(fullSmallAttackImage, 0, 0, al_get_bitmap_width(fullSmallAttackImage), al_get_bitmap_height(fullSmallAttackImage), fullSmallAttackPosX , fullSmallAttackPosY, al_get_bitmap_width(fullSmallAttackImage), al_get_bitmap_height(fullSmallAttackImage)*smallAttackScale, NULL);

}