#pragma once
#include <allegro5\allegro.h>
#include <vector>

#include "DEFINITIONS.h"


class BACKGROUND
{
public:
	BACKGROUND(ALLEGRO_DISPLAY *display);
	~BACKGROUND();
	void Draw(float cameraPosX);
	void Update(float posX);

private:
	ALLEGRO_BITMAP *backgroundImage;
	ALLEGRO_BITMAP *sourceBridgeImage;
	ALLEGRO_BITMAP *bridgeImage;
	float posX;
	float posY;

	void CreateBackgroundImage(ALLEGRO_DISPLAY *display);


};

