#pragma once
#include <allegro5\allegro.h>
#include <allegro5\keyboard.h>
#include <vector>
using namespace std;

class INPUT_MANAGER
{
public:
	INPUT_MANAGER();
	~INPUT_MANAGER();

	bool IsKeyPressed(ALLEGRO_EVENT event, int key);
	bool IsKeyPressed(ALLEGRO_EVENT event, vector<int> keys);

	bool IsKeyReleased(ALLEGRO_EVENT event, int key);
	bool IsKeyReleased(ALLEGRO_EVENT event, vector<int> keys);
};

