#include "GAME.h"

GAME::GAME(ALLEGRO_EVENT_QUEUE *event_queue, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *animationTimer, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER *timer1fps)
{
	display = al_create_display(SCREEN_WIDTH, SCREEN_HEIGHT);

	if (!display)
		al_show_native_message_box(NULL, NULL, NULL, "create allegro 5 window", NULL, NULL);

	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_set_window_title(display, "Dark Soldier");

	font = al_load_font("Fonts/batmfa.ttf", 34, NULL);
	gameOverImg = al_load_bitmap("Img/menuScreen.png");

	this->timer = timer;
	this->animationTimer = animationTimer;
	this->timer6fps = timer6fps;
	this->timer1fps = timer1fps;
	this->event_queue = event_queue;
	
	done = gameOver = false;

	background = new  BACKGROUND(display);
	player = new PLAYER(display);

	LoadMap();

	boss = NULL;
	bossIsActive = true;

	alpha = 0;
}

GAME::~GAME()
{
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);
	al_destroy_bitmap(gameOverImg);
	al_destroy_font(font);

	al_destroy_timer(timer);
	al_destroy_timer(animationTimer);
	al_destroy_timer(timer6fps);
	al_destroy_timer(timer1fps);


	delete player;
	delete background;
}



void GAME::Update()
{
	al_wait_for_event(event_queue, &ev);
	if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		done = true;

	//startScreen.Update();
	if(bossIsActive)
	SpawnBoss();

	if (gameOver || bossDied)
		GameOver();
	else
	{
		player->Update(ev, timer, animationTimer, gameOver);
		PlayerShoot();
		for (int i = 0; i < playerProjectiles.size(); i++)
			playerProjectiles[i]->Update(ev, timer);

		SpawnEnemy();
		for (int i = 0; i < 10; i++)
			if (groundEnemies[i] != NULL)
				groundEnemies[i]->Update(player, ev, timer, timer6fps, animationTimer, playerProjectiles);

		for (int i = 0; i < obstackles.size(); i++)
			obstackles[i]->Update(player, playerProjectiles, groundEnemies);


		for (int i = 0; i < platforms.size(); i++)
			platforms[i]->Update(player, ev, timer, timer6fps, animationTimer, playerProjectiles);

		camera.Update(player->posX, player->posY, player->getWidth(), player->getHeight());
		background->Update(camera.getCameraPos()[0]);

		// SPAWN SPINER

		if (input.IsKeyPressed(ev, ALLEGRO_KEY_ESCAPE))
			spinnerEnemies.push_back(new SPINNER_ENEMY(player->posX, player->posY));


		for (int i = 0; i < spinnerEnemies.size(); i++)
		{
			spinnerEnemies[i]->Update(player, camera.getCameraPos()[0], ev, timer, timer6fps, animationTimer, playerProjectiles, obstackles);

			if (!spinnerEnemies[i]->getIsActive())
			{
				player->addExperience(spinnerEnemies[i]->getExp());
				delete spinnerEnemies[i];
				spinnerEnemies.erase(spinnerEnemies.begin() + i);
			}
		}

		if(boss!=NULL)
			boss->Update(ev, animationTimer, player, playerProjectiles, bossDied, bossIsActive);

		powerup.Update(player, ev, timer1fps);

	}
}

void GAME::Draw()
{		
	if (al_is_event_queue_empty(event_queue))
	{
		background->Draw(camera.getCameraPos()[0]);

		for (int i = 0; i < platforms.size(); i++)
			platforms[i]->Draw();

		for (int i = 0; i < 10; i++)
			if (groundEnemies[i] != NULL)
				groundEnemies[i]->Draw();

		if (spinnerEnemies.size() > 0)
			spinnerEnemies[0]->Draw();

		if(boss!=NULL)
			boss->Draw(camera.getCameraPos()[0]);

		for (int i = 0; i < playerProjectiles.size(); i++)
			playerProjectiles[i]->Draw(player);

		player->Draw(camera.getCameraPos()[0]);

		for (int i = 0; i < obstackles.size(); i++)
			obstackles[i]->Draw();

		powerup.Draw(camera.getCameraPos()[0]);

		GameOverDraw();

		al_flip_display();
		//player->redraw = false;
	}
}

void GAME::SpawnEnemy()
{
	for (int i = 0; i < 10; i++)
	{
		int spawnPosX = 1500 + (1000 * i);

		if (((player->posX - spawnPosX < -2000) || (player->posX - spawnPosX > 2000)) && groundEnemies[i] == NULL)
  			groundEnemies[i] = new ENEMY2(spawnPosX, SCREEN_HEIGHT - 150, false);
	

		if (groundEnemies[i] != NULL)
			if (!groundEnemies[i]->getIsActive())
			{
				//player->addExperience(groundEnemies[i]->getExp());
				delete groundEnemies[i];
				groundEnemies[i] = NULL;
			}
	}
}

void GAME::PlayerShoot()
{
		if (input.IsKeyPressed(ev, ALLEGRO_KEY_SPACE))
			playerProjectiles.push_back(new PROJECTILE(player));

		for (int i = 0; i < playerProjectiles.size(); i++)
		{
			if (playerProjectiles[i]->getPosX() - playerProjectiles[i]->getStartPosX() > playerProjectiles[i]->getRange())
				playerProjectiles[i]->setIsActive(false);
			else if (playerProjectiles[i]->getPosX() - playerProjectiles[i]->getStartPosX() < -playerProjectiles[i]->getRange() + 100)
				playerProjectiles[i]->setIsActive(false);

			if (!playerProjectiles[i]->getIsActive())
			{
				delete playerProjectiles[i];
				playerProjectiles.erase(playerProjectiles.begin() + i);
			}
		}
}

void GAME::LoadMap()
{
	int loadCounterX = 0, loadCounterY = 0, mapSizeX, mapSizeY, map[50][50];

	for (int i = 0; i < platforms.size(); i++)
		delete platforms[i];
	platforms.clear();

	for (int i = 0; i < obstackles.size(); i++)
		delete obstackles[i];
	obstackles.clear();


	ifstream openfile("map.txt");

	if (openfile.is_open())
	{
		string line;
		getline(openfile, line);
		line.erase(remove(line.begin(), line.end(), ' '), line.end());
		mapSizeX = line.length();
		openfile.seekg(0, std::ios::beg);

		while (!openfile.eof())
		{
			openfile >> map[loadCounterX][loadCounterY];
			loadCounterX++;

			if (loadCounterX >= mapSizeX)
			{
				loadCounterX = 0;
				loadCounterY++;
			}

		}
		mapSizeY = loadCounterY;
	}

	openfile.close();

	for (int i = 0; i<mapSizeX; i++)
		for (int j = 0; j < mapSizeY; j++)
		{
			if (map[i][j] == 1)
				platforms.push_back(new PLATFORM(0 + i * 300, 0 + j * 50, 1));
			else if (map[i][j] == 2)
				platforms.push_back(new PLATFORM(0 + i * 300, 0 + j * 50, 2));
			else if (map[i][j] == 3)
				platforms.push_back(new PLATFORM(0 + i * 300, 0 + j * 50, 3));
			else if (map[i][j] == 4)
				platforms.push_back(new PLATFORM(0 + i * 300, 0 + j * 50, 4));
			else if (map[i][j] == 5)
				platforms.push_back(new PLATFORM(0 + i * 300, 0 + j * 50, 5));
			else if (map[i][j] == 6)
				platforms.push_back(new PLATFORM(0 + i * 300, 0 + j * 50, 6));
			else if (map[i][j] == 9)
				obstackles.push_back(new OBSTACKLE(70 + i * 300));
		}

}

void GAME::GameOver()
{
	if (input.IsKeyPressed(ev, ALLEGRO_KEY_ENTER))
	{
		delete player;
		player = new PLAYER(display);

		LoadMap();
		
		for (int i = 1; i < 10; i++)
		{
			delete groundEnemies[i];
			groundEnemies[i] = NULL;
		}

		gameOver = false;
		alpha = 0;
		bossDied = false;
		bossIsActive = true;
	}
	if (ev.type == ALLEGRO_EVENT_TIMER)
		if (ev.timer.source == animationTimer)
			if (alpha < 250)
				alpha += 10;


}

void GAME::SpawnBoss()
{
	if (player->posX >= 11500 && boss == NULL)
		boss = new BOSS();
	else if (player->posX < 11500 && boss != NULL)
	{
		delete boss;
		boss = NULL;
	}
}

void GAME::GameOverDraw()
{
	if (gameOver)
	{
		al_draw_filled_rectangle(camera.getCameraPos()[0], 0, camera.getCameraPos()[0] + SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
		al_draw_text(font, al_map_rgb(255, 255, 255), camera.getCameraPos()[0] + SCREEN_WIDTH / 2 - al_get_text_width(font, "You are dead!") / 2, SCREEN_HEIGHT / 2 - 80, NULL, "You are dead!");
		al_draw_text(font, al_map_rgb(255, 255, 255), camera.getCameraPos()[0] + SCREEN_WIDTH / 2 - al_get_text_width(font, "Press ENTER to try again") / 2, SCREEN_HEIGHT / 2, NULL, "Press ENTER to try again");
	}
	else if (bossDied)
	{
		al_draw_filled_rectangle(camera.getCameraPos()[0], 0, camera.getCameraPos()[0] + SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
		al_draw_text(font, al_map_rgb(255, 255, 255), camera.getCameraPos()[0] + SCREEN_WIDTH / 2 - al_get_text_width(font, "Congratulations! You have defeated the boss") / 2, SCREEN_HEIGHT / 2 - 80, NULL, "Congratulations! You have defeated the boss");
		al_draw_text(font, al_map_rgb(255, 255, 255), camera.getCameraPos()[0] + SCREEN_WIDTH / 2 - al_get_text_width(font, "If you wish to try again press ENTER") / 2, SCREEN_HEIGHT / 2, NULL, "If you wish to try again press ENTER");
	}
}

//################################################################################
bool GAME::getDone()
{
	return done;
}

