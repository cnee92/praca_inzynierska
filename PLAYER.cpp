#include "PLAYER.h"

#include <iostream>
PLAYER::PLAYER()
{}

PLAYER::PLAYER(ALLEGRO_DISPLAY *display)
{
	int imageFrameX = 11;
	int imageFrameY = 2;

	image = al_load_bitmap("Img/player.png");
	hpBorderImg = al_load_bitmap("Img/hpBar.png");
	font = al_load_font("Fonts/batmfa.ttf", 15, NULL);

	width = al_get_bitmap_width(image) / imageFrameX;
	height = al_get_bitmap_height(image) / imageFrameY;

	animationActive = false;
	imageSourceX = imageSourceY = 0;

	posX = 10;
	posY = groundY = SCREEN_HEIGHT - 150 - height;
	velY = 0;

	bboxSizeX = 30;
	bbox = new BOUNDINGBOX(posX + bboxSizeX, posY, width - 2*bboxSizeX, height);
	prevBbox = new BOUNDINGBOX(posX + bboxSizeX, posY, width - 2*bboxSizeX, height);

	moveSpeed = 7;
	jumpSpeed = 20;
	dmg = 5;
	isDDActive = false;
	isShieldActive = false;

	isActive = isAlive = true;

	jump = onSolid = jumpKeyPressed = squat = false;

	redraw = true;

	projectileDirRight = 1;
	projectileDirLeft = 0;

	this->hpBorder = 300;
	this->maxHp = 200;
	this->currentHp = maxHp;
	this->hpBar = 300;
	
	this->experience = 0;
	this->expToLvl = 50;
	this->lvlBar = 300;
	this->lvlUp = false;
	this->lvl = 0;
	this->scale = 1.0f;
	lvlUpImg = al_load_bitmap("Img/lvlUP.png");
	lvlUpWidth = al_get_bitmap_width(lvlUpImg);
	lvlUpHeight = al_get_bitmap_height(lvlUpImg);
}


PLAYER::~PLAYER()
{
	al_destroy_bitmap(image);
	al_destroy_bitmap(hpBorderImg);

	bbox = NULL;
	prevBbox = NULL;
}

void PLAYER::Update(ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *animationTimer, bool &gameOver)
{
	if (currentHp <= 0)
		isAlive = false;

	if (!isActive)
		gameOver = true;

	hpText = to_string((int)currentHp) + " / " + to_string((int)maxHp);
	expText = to_string((int)experience) + " / " + to_string((int)expToLvl);

	if (isAlive)
	{
		prevPosX = posX;
		prevPosY = posY;

		CheckInput(ev);

		Move(ev, timer, animationTimer);

		LvlUP();

		delete bbox;
		delete prevBbox;

		if (!dirKeys[SQUAT])
		{
			projectilePosY = posY + 15;

			bbox = new BOUNDINGBOX(posX + bboxSizeX, posY, width - 2 * bboxSizeX, height);
			prevBbox = new BOUNDINGBOX(prevPosX + bboxSizeX, prevPosY, width - 2 * bboxSizeX, height);
		}
		else
		{
			projectilePosY = posY + 45;

			bbox = new BOUNDINGBOX(posX + bboxSizeX, posY + 35, al_get_bitmap_width(image) - 2 * bboxSizeX, al_get_bitmap_height(image) / 2);
			prevBbox = new BOUNDINGBOX(prevPosX + bboxSizeX, prevPosY + 35, al_get_bitmap_width(image) - 2 * bboxSizeX, al_get_bitmap_height(image) / 2);
		}
	}

	if (!isAlive && !deathExplosion)
	{
		image = al_load_bitmap("Img/dead.png");
		width = al_get_bitmap_width(image) / 9;
		newHeight = al_get_bitmap_height(image);
		imageSourceX = 0;
		imageSourceY = 0;
		deathExplosion = true;
	}

	if (!isAlive)
		if (ev.type == ALLEGRO_EVENT_TIMER)
			if (ev.timer.source == animationTimer)
				imageSourceX += width;

	if (imageSourceX >= al_get_bitmap_width(image))
			isActive = false;

}


void PLAYER::Draw(float cameraPosX)
{
	if (lvlUp)
	{
		lvlUpPosX = cameraPosX + SCREEN_WIDTH / 2 -  (lvlUpWidth*(scale / 2));
		lvlUpPosY = SCREEN_HEIGHT / 2 - (lvlUpHeight / 2 - (lvlUpHeight*(scale / 2)));
		al_draw_scaled_bitmap(lvlUpImg, 0, 0, al_get_bitmap_width(lvlUpImg), al_get_bitmap_height(lvlUpImg), lvlUpPosX, lvlUpPosY, al_get_bitmap_width(lvlUpImg)*scale, al_get_bitmap_height(lvlUpImg)*scale, NULL);
	}

	if (isAlive)
	{
		if (!dirKeys[SQUAT])
		{
			//bbox->Draw();
			al_draw_bitmap_region(image, imageSourceX, imageSourceY * height, width, height, posX, posY, NULL);
			DrawBars(cameraPosX);
		}
		else
		{
			//bbox->Draw();
			al_draw_bitmap_region(image, imageSourceX, imageSourceY * 80, 80, 80, posX, posY + 35, NULL);
			DrawBars(cameraPosX);
		}
	}
	else
		al_draw_bitmap_region(image, imageSourceX, imageSourceY, width, newHeight, posX + 10, posY + (height - newHeight), NULL);

}

void PLAYER::Move(ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *animationTimer)
{
		if (ev.type == ALLEGRO_EVENT_TIMER)
		{
			if (ev.timer.source == timer)
			{
				if (dirKeys[0] || dirKeys[1])
					animationActive = true;
				else
					animationActive = false;

				if (!dirKeys[SQUAT])
				{
				if (posX > 0)
					posX -= dirKeys[LEFT] * moveSpeed;

				if (posX<12700+SCREEN_WIDTH-110)
				posX += dirKeys[RIGHT] * moveSpeed;
				}

				Jump();

				redraw = true;
			}

			if (ev.timer.source == animationTimer)
			{
				if (animationActive && !squat)
					imageSourceX += width;
				else
					imageSourceX = 0;

				if (imageSourceX >= al_get_bitmap_width(image) - 10)
					imageSourceX = 97;
			}
		}
}


void PLAYER::CheckInput(ALLEGRO_EVENT ev)
{
	//KEY PRESSED
	if (input.IsKeyPressed(ev, ALLEGRO_KEY_LEFT))
	{
		imageSourceY = LEFT;
		dirKeys[LEFT] = true;

		projectileDirLeft = projectileDirRight = 0;
		projectileDirLeft = 1;
	}
	
	if (input.IsKeyPressed(ev, ALLEGRO_KEY_RIGHT))
	{
		imageSourceY = RIGHT;
		dirKeys[RIGHT] = true;

		projectileDirLeft = projectileDirRight = 0;
		projectileDirRight = 1;
	}

	if (input.IsKeyPressed(ev, ALLEGRO_KEY_UP))
	{
		dirKeys[JUMP] = true;
		jumpKeyPressed = true;

	}

	if (input.IsKeyPressed(ev, ALLEGRO_KEY_DOWN))
	{
		dirKeys[SQUAT] = true;
		squat = true;
		imageSourceX = 0;
		if(!isDDActive)
			image = al_load_bitmap("Img/playerSquat.png");
		else
			image = al_load_bitmap("Img/playerSquatDD.png");
	}


	//KEY RELEASED
	if (input.IsKeyReleased(ev, ALLEGRO_KEY_LEFT))
		dirKeys[LEFT] = false;

	if (input.IsKeyReleased(ev, ALLEGRO_KEY_RIGHT))
		dirKeys[RIGHT] = false;

	if (input.IsKeyReleased(ev, ALLEGRO_KEY_UP))
	{
		dirKeys[JUMP] = false;
		jumpKeyPressed = false;
	}
	if (input.IsKeyReleased(ev, ALLEGRO_KEY_DOWN))
	{
		if (!isDDActive)
			image = al_load_bitmap("Img/player.png");
		else
			image = al_load_bitmap("Img/playerDD.png");
		dirKeys[SQUAT] = false;
		squat = false;
	}

}

void PLAYER::Jump()
{

	if (onSolid && !jumpKeyPressed)
		jump = true;

	if (squat)
		jump = false;

	if (posY >= groundY && !onSolid)
	{
		velY = 0;
		onSolid = true;
		posY = groundY;
	}

	if (dirKeys[JUMP] && jump)
	{
		jump = false;
		velY = -jumpSpeed;
		onSolid = false;
	}

	if (!onSolid)
	{
		jump = false;
		posY += velY;
		velY += GRAVITY;
	}
}


void PLAYER::DrawBars(float posX)
{
	posX += 10;
	hpBar = hpBorder * (currentHp / maxHp);
	char slashSize = al_get_text_width(font, "/");


	lvlBar = (experience / expToLvl) * hpBorder;
	if (lvlBar > hpBorder)
		lvlBar = hpBorder;


	if(currentHp>0)
		al_draw_filled_rectangle(posX+7, SCREEN_HEIGHT - 100 + 10, posX + 7 + hpBar, SCREEN_HEIGHT - 80 + 7, al_map_rgb(255, 0, 0));
	al_draw_bitmap(hpBorderImg, posX, SCREEN_HEIGHT - 100, NULL);
	al_draw_text(font, al_map_rgb(255, 255, 255), posX + al_get_bitmap_width(hpBorderImg) / 2 - slashSize/2, SCREEN_HEIGHT - 100, NULL, "/");
	al_draw_text(font, al_map_rgb(255, 255, 255), posX + al_get_bitmap_width(hpBorderImg) / 2 - al_get_text_width(font, to_string((int)currentHp).c_str()) - slashSize, SCREEN_HEIGHT - 100, NULL, to_string((int)currentHp).c_str());
	al_draw_text(font, al_map_rgb(255, 255, 255), posX + al_get_bitmap_width(hpBorderImg) / 2 + slashSize, SCREEN_HEIGHT - 100, NULL, to_string((int)maxHp).c_str());


		al_draw_filled_rectangle(posX + 7, SCREEN_HEIGHT - 65 + 10, posX + 7 + lvlBar, SCREEN_HEIGHT - 45 + 7, al_map_rgb(0, 162, 232));
	al_draw_bitmap(hpBorderImg, posX, SCREEN_HEIGHT - 65, NULL);
	al_draw_text(font, al_map_rgb(255, 255, 255), posX + al_get_bitmap_width(hpBorderImg) / 2 - slashSize / 2, SCREEN_HEIGHT - 65, NULL, "/");
	al_draw_text(font, al_map_rgb(255, 255, 255), posX + al_get_bitmap_width(hpBorderImg) / 2 - al_get_text_width(font, to_string((int)experience).c_str()) - slashSize, SCREEN_HEIGHT - 65, NULL, to_string((int)experience).c_str());
	al_draw_text(font, al_map_rgb(255, 255, 255), posX + al_get_bitmap_width(hpBorderImg) / 2 + slashSize, SCREEN_HEIGHT - 65, NULL, to_string((int)expToLvl).c_str());
	
	string lvlText = "Level: " + to_string((int)lvl);
	al_draw_text(font, al_map_rgb(255, 255, 255), posX + al_get_bitmap_width(hpBorderImg) / 2 - al_get_text_width(font, lvlText.c_str())/2, SCREEN_HEIGHT - 30, NULL, lvlText.c_str());

}

void PLAYER::LvlUP()
{
	if (experience >= expToLvl)
	{
		lvl++;
		experience = experience - expToLvl;
		expToLvl = 100 * lvl * 1.2;
		maxHp = 200 + lvl * 100;
		currentHp = maxHp;
		dmg += 2;
		lvlUp = true;
	}

	if (lvlUp)
		scale -= 0.01f;

	if (scale <= 0)
	{
		lvlUp = false;
		scale = 1;
	}
}

//################################################################
void PLAYER::addExperience(int exp)
{
	this->experience += exp;
}

float PLAYER::getWidth()
{
	return width;
}

float PLAYER::getHeight()
{
	return height;
}

bool PLAYER::getOnSolid()
{
	return onSolid;
}

void PLAYER::setOnSolid(bool onSolid)
{
	this->onSolid = onSolid;
}

float PLAYER::getBboxSizeX()
{
	return bboxSizeX;
}

float PLAYER::getCurrentHp()
{
	return currentHp;
}

void PLAYER::subCurrentHp(float value)
{
	if(!isShieldActive)
		this->currentHp -= value;
}

float PLAYER::getMaxHP()
{
	return maxHp;
}

int PLAYER::getDmg()
{
	return dmg;
}

void PLAYER::setDmg(int dmg)
{
	this->dmg = dmg;
}

void PLAYER::LoadPlayerImg(const char* fileName)
{
	image = al_load_bitmap(fileName);
}

bool PLAYER::getIsDDActive()
{
	return isDDActive;
}

void PLAYER::setIsDDActive(bool isDDActive)
{
	this->isDDActive = isDDActive;
}

bool PLAYER::getSquat()
{
	return squat;
}

bool PLAYER::getIsShieldActive()
{
	return isShieldActive;
}

void PLAYER::setIsShieldActive(bool isShieldActive)
{
	this->isShieldActive = isShieldActive;
}

float PLAYER::getLvl()
{
	return lvl;
}