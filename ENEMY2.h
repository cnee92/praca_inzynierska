#pragma once
#include "ENEMY.h"
class ENEMY2 :
	public ENEMY
{
public:
	ENEMY2(float posX, float posY, bool isOnPlatform);
	~ENEMY2();
private:
	void SpawnProjectile(ALLEGRO_EVENT ev, PLAYER* player, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER* timer);

};

