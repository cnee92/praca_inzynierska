#pragma once

#include <allegro5\allegro_primitives.h>

class BOUNDINGBOX
{
public:
	BOUNDINGBOX(float x, float y, float w, float h);
	~BOUNDINGBOX();

	bool Intersects(BOUNDINGBOX *obj);
	void Draw();

	const float left, right, top, bottom;
};

