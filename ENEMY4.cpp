#include "ENEMY4.h"



ENEMY4::ENEMY4(float posX, float posY, bool isOnPlatform)
{

	float imageFrameX = 6;
	float imageFrameY = 2;

	image = al_load_bitmap("Img/Cov4.png");
	this->width = al_get_bitmap_width(image) / imageFrameX;
	this->height = al_get_bitmap_height(image) / imageFrameY;


	this->posX = this->prevPosX = this->startPosX = (rand() % 280) + posX;
	this->posY = posY - height;
	this->bboxSizeX = 60;

	this->exp = 25;
	this->hpBorder = 100;
	this->maxHp = 200;
	this->currentHp = maxHp;
	this->hpBar = 100;
//	this->hpBarPart = 100 / maxHp;

	bbox = new BOUNDINGBOX(posX + bboxSizeX, posY, width - bboxSizeX, height);

	this->projectleTime = 0;
	moveSpeed = 2;
	isActive = isAlive = true;
	deathExplosion = false;

	this->isOnPlatform = isOnPlatform;
	this->limitPosX = posX + 10;

	if ((rand() % 10) < 5)
		onPlatformDir = false;
	else
		onPlatformDir = true;
}


ENEMY4::~ENEMY4()
{
}

void ENEMY4::SpawnProjectile(ALLEGRO_EVENT ev, PLAYER* player, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER* timer)
{
	if (ev.type == ALLEGRO_EVENT_TIMER)
		if (ev.timer.source == timer6fps)
		{

			if (!isOnPlatform)
			{
				if (projectleTime < 3)
					projectiles.push_back(new ENEMY_PROJECTILE(player->posX, posX, posY, width, height));
			}
			else
				if (projectleTime < 3)
					projectiles.push_back(new ENEMY_PROJECTILE(onPlatformDir, posX, posY, width, height));

			projectleTime++;

			if (projectleTime >= 3)
				projectleTime = 0;
		}

	for (int i = 0; i < projectiles.size(); i++)
	{
		projectiles[i]->Update(player, ev, timer);

		if (projectiles[i]->getPosX() > startPosX + SCREEN_WIDTH / 1.5)
			projectiles[i]->setIsActive(false);
		else if (projectiles[i]->getPosX() < startPosX - SCREEN_WIDTH / 1.5)
			projectiles[i]->setIsActive(false);

		if (!projectiles[i]->getIsActive())
		{
			delete projectiles[i];
			projectiles.erase(projectiles.begin() + i);
		}
	}
}
