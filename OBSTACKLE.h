#pragma once
#include "PLATFORM.h"
class OBSTACKLE :
	public PLATFORM
{
public:
	OBSTACKLE(float posX);
	~OBSTACKLE();

	void Update(PLAYER *player, vector<PROJECTILE*> &playerProjectile, ENEMY *groundEnemy[10]);
	void Draw();
private:
	void Collision(PLAYER *player, vector<PROJECTILE*> &playerProjectile, ENEMY *groundEnemy[10]);
};

