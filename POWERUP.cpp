#include "POWERUP.h"

int i;

POWERUP::POWERUP()
{
	font = al_load_font("Fonts/Lato.ttf", 40, NULL);
	font1 = al_load_font("Fonts/Lato.ttf", 14, NULL);

	healingButton = al_load_bitmap("Img/healButton.png");

	healingAvailable = false;
	healingStart = false;
	healingReady = true;
	healingCD = 10;

	healingPosX = SCREEN_WIDTH - 300;
	healingPosY = SCREEN_HEIGHT - 100;
	healingButtonWidth = al_get_bitmap_width(healingButton);
	healingButtonHeight = al_get_bitmap_height(healingButton);
	//####//

	//SHIELD//
	shieldButton = al_load_bitmap("Img/shieldButton.png");
	shieldImage = al_load_bitmap("Img/barier.png");
	shieldAvailable = false;
	shieldReady = true;
	shieldActive = false;
	shieldDuration = 10;
	shieldCD = 25;

	shieldPosX = healingPosX + healingButtonWidth + 30;
	shieldPosY = healingPosY;
	shieldButtonWidth = al_get_bitmap_width(shieldButton);
	shieldButtonHeight = al_get_bitmap_height(shieldButton);
	shieldBBox = new BOUNDINGBOX(1, 1, 1, 1);
	//####//

	//DOUBLE DMG//
	ddButton = al_load_bitmap("Img/ddButton.png");
	ddReady = true;
	ddAvailable = false;

	ddDuration = 10;
	ddCD = 25;

	ddPosX = shieldPosX + healingButtonWidth  + 30;
	ddPosY = shieldPosY;
	ddButtonWidth = al_get_bitmap_width(ddButton);
	ddButtonHeight = al_get_bitmap_height(ddButton);
	//####//
}


POWERUP::~POWERUP()
{
}

void POWERUP::Update(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer1fps)
{
	CheckInput(player, ev);
	Healing(player, ev, timer1fps);
	DoubleDmg(player, ev, timer1fps);
	Shield(player, ev, timer1fps);

}

void POWERUP::Draw(float cameraPosX)
{
	DrawHeling(cameraPosX);
	DrawDoubleDmg(cameraPosX);
	DrawShield(cameraPosX);
}


void POWERUP::DrawHeling(float cameraPosX)
{
	al_draw_bitmap(healingButton, cameraPosX + healingPosX, healingPosY, NULL);

	if (!healingReady)
	{
		al_draw_filled_rectangle(cameraPosX + healingPosX, healingPosY, cameraPosX + healingPosX + healingButtonWidth, healingPosY + healingButtonHeight, al_map_rgba(0, 0, 0, 128));
		al_draw_text(font, al_map_rgb(255, 255, 255), cameraPosX + healingPosX + healingButtonWidth / 2 - al_get_text_width(font, healingTimeText.c_str()) / 2, healingPosY + healingButtonHeight / 2 - 23, NULL, healingTimeText.c_str());
	}

	if (!healingAvailable)
	{
		al_draw_filled_rectangle(cameraPosX + healingPosX, healingPosY, cameraPosX + healingPosX + healingButtonWidth, healingPosY + healingButtonHeight, al_map_rgba(0, 0, 0, 128));
		al_draw_text(font1, al_map_rgb(255, 255, 255), cameraPosX + healingPosX + healingButtonWidth / 2 - al_get_text_width(font1, healingTimeText.c_str()) / 2, healingPosY + healingButtonHeight / 2 - 7, NULL, healingTimeText.c_str());
	}
}

void POWERUP::DrawDoubleDmg(float cameraPosX)
{
	al_draw_bitmap(ddButton, cameraPosX + ddPosX, ddPosY, NULL);

	if (!ddReady)
	{
		al_draw_filled_rectangle(cameraPosX + ddPosX, ddPosY, cameraPosX + ddPosX + ddButtonWidth, ddPosY + ddButtonHeight, al_map_rgba(0, 0, 0, 128));
		al_draw_text(font, al_map_rgb(255, 255, 255), cameraPosX + ddPosX + ddButtonWidth / 2 - al_get_text_width(font, ddTimeText.c_str()) / 2, ddPosY + ddButtonHeight / 2 - 23, NULL, ddTimeText.c_str());
	}

	if (!ddAvailable)
	{
		al_draw_filled_rectangle(cameraPosX + ddPosX, ddPosY, cameraPosX + ddPosX + ddButtonWidth, ddPosY + ddButtonHeight, al_map_rgba(0, 0, 0, 128));
		al_draw_text(font1, al_map_rgb(255, 255, 255), cameraPosX + ddPosX + ddButtonWidth / 2 - al_get_text_width(font1, ddTimeText.c_str()) / 2, ddPosY + ddButtonHeight / 2 - 7, NULL, ddTimeText.c_str());
	}

}

void POWERUP::DrawShield(float cameraPosX)
{
	al_draw_bitmap(shieldButton, cameraPosX + shieldPosX, shieldPosY, NULL);

	if (!shieldReady)
	{
		al_draw_filled_rectangle(cameraPosX + shieldPosX, shieldPosY, cameraPosX + shieldPosX + shieldButtonWidth, shieldPosY + shieldButtonHeight, al_map_rgba(0, 0, 0, 128));
		al_draw_text(font, al_map_rgb(255, 255, 255), cameraPosX + shieldPosX + shieldButtonWidth / 2 - al_get_text_width(font, shieldTimeText.c_str()) / 2, shieldPosY + shieldButtonHeight / 2 - 23, NULL, shieldTimeText.c_str());
	}

	if (shieldActive)
	{
		al_draw_bitmap(shieldImage, shieldImgPosX, shieldImgPosY, NULL);
		//shieldBBox->Draw();
	}

	if (!shieldAvailable)
	{
		al_draw_filled_rectangle(cameraPosX + shieldPosX, shieldPosY, cameraPosX + shieldPosX + shieldButtonWidth, shieldPosY + shieldButtonHeight, al_map_rgba(0, 0, 0, 128));
		al_draw_text(font1, al_map_rgb(255, 255, 255), cameraPosX + shieldPosX + shieldButtonWidth / 2 - al_get_text_width(font1, shieldTimeText.c_str()) / 2, shieldPosY + shieldButtonHeight / 2 - 7, NULL, shieldTimeText.c_str());
	}


}

void POWERUP::CheckInput(PLAYER *player, ALLEGRO_EVENT ev)
{


	if (input.IsKeyPressed(ev, ALLEGRO_KEY_Q))
		if (healingReady && healingAvailable)
		{
			healLimit = player->getMaxHP() / 3;
			healingStart = true;
			healingReady = false;
		}

	if (input.IsKeyPressed(ev, ALLEGRO_KEY_E))
		if (ddReady && ddAvailable)
		{
			player->setDmg(player->getDmg() * 2);

			if(!player->getSquat())
 				player->LoadPlayerImg("Img/playerDD.png");
			else
				player->LoadPlayerImg("Img/playerSquatDD.png");
			player->setIsDDActive(true);
			ddReady = false;
		}

	if (input.IsKeyPressed(ev, ALLEGRO_KEY_W))
		if (shieldReady && shieldAvailable)
		{
			player->setIsShieldActive(true);
			shieldReady = false;
			shieldActive = true;
			shieldDuration = 10;
		}
}


void POWERUP::Healing(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer1fps)
{
	if (player->getLvl() == 2 && !healingAvailable)
		healingAvailable = true;

	if (!healingAvailable)
		healingTimeText = "LVL 2";
	else
		healingTimeText = to_string(healingCD);



	if (healingStart)
	{
		if (healedPoint <= healLimit && player->getCurrentHp() < player->getMaxHP())
		{
			player->subCurrentHp(-1);
			healedPoint++;
		}
		else
		{
			healedPoint = 0;
			healingStart = false;
		}
	}

	if (!healingReady)
		if (ev.type == ALLEGRO_EVENT_TIMER)
			if (ev.timer.source == timer1fps)
				healingCD--;

	if (healingCD == 0)
	{
		healingReady = true;
		healingCD = 10;
	}
}

void POWERUP::DoubleDmg(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer1fps)
{
	if (player->getLvl() == 5 && !ddAvailable)
		ddAvailable = true;

	if (!ddAvailable)
		ddTimeText = "LVL 5";
	else
		ddTimeText = to_string(ddCD);

	if (player->getIsDDActive())
		if (ev.type == ALLEGRO_EVENT_TIMER)
			if (ev.timer.source == timer1fps)
				ddDuration--;


	if (ddDuration == 0)
	{
		if(!player->getSquat())
			player->LoadPlayerImg("Img/player.png");
		else
			player->LoadPlayerImg("Img/playerSquat.png");
		player->setDmg(player->getDmg()/2);
		ddDuration = 10;
		player->setIsDDActive(false);
	}

	if (!ddReady)
		if (ev.type == ALLEGRO_EVENT_TIMER)
			if (ev.timer.source == timer1fps)
				ddCD--;

	if (ddCD == 0)
	{
		ddReady = true;
		ddCD = 25;
	}

	
}

void POWERUP::Shield(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer1fps)
{
	if (player->getLvl() == 8 && !shieldAvailable)
		shieldAvailable = true;

	if (!shieldAvailable)
		shieldTimeText = "LVL 8";
	else
		shieldTimeText = to_string(shieldCD);



	if (shieldActive)
	{
		shieldImgPosX = player->posX + player->getWidth() / 2 - al_get_bitmap_width(shieldImage) / 2;
		shieldImgPosY = player->posY + player->getHeight() / 2 - al_get_bitmap_height(shieldImage) / 2 - 10;
		delete shieldBBox;
		shieldBBox = new BOUNDINGBOX(shieldImgPosX, shieldImgPosY, al_get_bitmap_width(shieldImage), al_get_bitmap_height(shieldImage));
	}


	if (shieldActive)
		if (ev.type == ALLEGRO_EVENT_TIMER)
			if (ev.timer.source == timer1fps)
				shieldDuration--;


	if (shieldDuration == 0)
	{
		shieldActive = false;
		player->setIsShieldActive(false);
		shieldDuration = 10;
	}

	if (!shieldReady)
		if (ev.type == ALLEGRO_EVENT_TIMER)
			if (ev.timer.source == timer1fps)
				shieldCD--;

	if (shieldCD == 0)
	{
		shieldReady = true;
		shieldCD = 25;
	}
}