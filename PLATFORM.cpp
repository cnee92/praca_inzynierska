#include "PLATFORM.h"

PLATFORM::PLATFORM()
{}

PLATFORM::PLATFORM(float posX, float posY, int type)
{
	image = al_load_bitmap("Img/platform.png");

	this->posX = posX;
	this->posY = posY;
	width = al_get_bitmap_width(image);
	height = al_get_bitmap_height(image);;
	this->type = type;

	bbox = new BOUNDINGBOX(posX, posY+15, width, 10);

	onPlatform = false;
	enemy = NULL;
	enemy2 = NULL;
	enemy3 = NULL;
	enemy4 = NULL;


	int random = 1;//rand() % 10;
	if (random < 4)
	{
		if (type == 3)
			enemy = new ENEMY(posX, posY + 10, true);
		else if (type == 4)
			enemy = new ENEMY2(posX, posY + 10, true);
		else if (type == 5)
			enemy = new ENEMY3(posX, posY + 10, true);
		else if (type == 6)
			enemy = new ENEMY4(posX, posY + 10, true);
	}
}


PLATFORM::~PLATFORM()
{

}

void PLATFORM::Update(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER *animationTimer, std::vector<PROJECTILE*> &playerProjectiles)
{
	Collision(player);

	SpawnEnemy(player, ev, timer, timer6fps, animationTimer, playerProjectiles);

}

void PLATFORM::Draw()
{
	
	if (enemy != NULL)
		enemy->Draw();
	al_draw_bitmap(image, posX, posY, NULL);
	//bbox->Draw();
}


void PLATFORM::Collision(PLAYER* player)
{
	if (onPlatform && (player->bbox->right < bbox->left || player->bbox->left > bbox->right))
		player->velY = 1;

	if (player->bbox->Intersects(bbox))
	{
		if (player->bbox->bottom >= bbox->top && player->prevBbox->bottom <= bbox->top)
		{
			onPlatform = true;
			player->setOnSolid(true);
			player->posY = bbox->top - player->getHeight();
		}

		else if (player->bbox->top <= bbox->bottom && player->prevBbox->top >= bbox->bottom)
		{
			player->velY = 2;
			player->posY = bbox->top + 5;
		}

		else if (player->bbox->right >= bbox->left && player->prevBbox->right <= bbox->left)
			player->posX = (bbox->left - player->getWidth()+player->getBboxSizeX());

		else if (player->bbox->left <= bbox->right && player->prevBbox->left >= bbox->right)
			player->posX = (bbox->left + width-player->getBboxSizeX());
	}
	else if (!(player->bbox->Intersects(bbox)) && onPlatform)
	{
		onPlatform = false;
		player->setOnSolid(false);
	}
}

void PLATFORM::SpawnEnemy(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER *animationTimer, std::vector<PROJECTILE*> &playerProjectiles)
{
	if (enemy != NULL)
	{
		enemy->Update(player, ev, timer, timer6fps, animationTimer, playerProjectiles);
		if (!enemy->getIsActive())
		{
			delete enemy;
			enemy = NULL;
		}
	}

	if ((player->posX - posX < -2000) || (player->posX - posX > 2000))
	{
		if (type == 3 && enemy == NULL)
			enemy = new ENEMY(posX, posY + 10, true);
		else if (type == 4 && enemy == NULL)
			enemy = new ENEMY2(posX, posY + 10, true);
		else if (type == 5 && enemy == NULL)
			enemy = new ENEMY3(posX, posY + 10, true);
		else if (type == 6 && enemy == NULL)
			enemy = new ENEMY4(posX, posY + 10, true);
	}
}

float PLATFORM::getPosX()
{
	return posX;
}
float PLATFORM::getPosY()
{
	return posY;
}
float PLATFORM::getWidth()
{
	return width;
}
float PLATFORM::getHeight()
{
	return height;
}