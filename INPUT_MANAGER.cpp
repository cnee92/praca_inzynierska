#include "INPUT_MANAGER.h"



INPUT_MANAGER::INPUT_MANAGER()
{
}


INPUT_MANAGER::~INPUT_MANAGER()
{
}

bool INPUT_MANAGER::IsKeyPressed(ALLEGRO_EVENT event, int key)
{
	if (event.type == ALLEGRO_EVENT_KEY_DOWN)
		if (event.keyboard.keycode == key)
			return true;

	return false;
}
bool INPUT_MANAGER::IsKeyPressed(ALLEGRO_EVENT event, vector<int> keys)
{
	if (event.type == ALLEGRO_EVENT_KEY_DOWN)
		for (int i = 0; i < keys.size(); i++)
			if (event.keyboard.keycode == keys[i])
				return true;

	return false;
}

bool INPUT_MANAGER::IsKeyReleased(ALLEGRO_EVENT event, int key)
{
	if (event.type == ALLEGRO_EVENT_KEY_UP)
		if (event.keyboard.keycode == key)
			return true;

	return false;
}

bool INPUT_MANAGER::IsKeyReleased(ALLEGRO_EVENT event, vector<int> keys)
{
	if (event.type == ALLEGRO_EVENT_KEY_UP)
		for (int i = 0; i < keys.size(); i++)
			if (event.keyboard.keycode == keys[i])
				return true;

	return false;
}