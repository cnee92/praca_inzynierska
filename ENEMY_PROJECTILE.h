#pragma once
#include "PROJECTILE.h"


class ENEMY_PROJECTILE : public PROJECTILE
{
public:
	ENEMY_PROJECTILE();
	ENEMY_PROJECTILE(float playerPosX, float posX, float posY, int width, int height);
	ENEMY_PROJECTILE(bool dir, float posX, float posY, int width, int height);
	~ENEMY_PROJECTILE();
	
	

	void Update(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer);
	void Draw();

private:
	char dir; // 0 - left   1 - right;
	bool isShieldActive;

	void Move(ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer);
	void Collision(PLAYER *player);

};

