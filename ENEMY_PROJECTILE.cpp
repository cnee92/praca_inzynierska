#include "ENEMY_PROJECTILE.h"

ENEMY_PROJECTILE::ENEMY_PROJECTILE()
{
}

ENEMY_PROJECTILE::ENEMY_PROJECTILE(float playerPosX, float posX, float posY, int width, int height)
{
	image = al_load_bitmap("Img/redProjectile.png");

	if (playerPosX - posX < 0)
	{
		dir = 0;
		this->posX = posX;
	}
	else
	{
		dir = 1;
		this->posX = posX + width / 2;
	}
	
	//cout << posY << endl;
	this->posY = posY + height/2-5;
	this->width = al_get_bitmap_width(image);
	this->height = al_get_bitmap_height(image);
	this->startPosX = posX;
	this->moveSpeed = 10;
	this->isActive = this->isAlive = true;
	this->dmg = 10;
}

ENEMY_PROJECTILE::ENEMY_PROJECTILE(bool dir, float posX, float posY, int width, int height)
{
	image = al_load_bitmap("Img/redProjectile.png");

	if (!dir)
		this->posX = posX;
	else
		this->posX = posX + width / 2;
	
	this->dir = dir;
	this->posY = posY + (height / 2) + 2;
	this->width = al_get_bitmap_width(image);
	this->height = al_get_bitmap_height(image);
	this->startPosX = posX;
	this->moveSpeed = 10;
	this->isActive = this->isAlive = true;
	this->dmg = 10;
}

ENEMY_PROJECTILE::~ENEMY_PROJECTILE()
{
}


void ENEMY_PROJECTILE::Update(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer)
{
	if (isAlive)
	{
		delete bbox;
		bbox = new BOUNDINGBOX(posX, posY, width, height);
		Collision(player);
		Move(ev, timer);
		isShieldActive = player->getIsShieldActive();
	}
}

void ENEMY_PROJECTILE::Draw()
{
	if (isAlive)
		al_draw_bitmap(image, posX, posY, NULL);
	else
		if (!isShieldActive)
		StartExplosion("Img/redprojectileExplo.png");
}

void ENEMY_PROJECTILE::Move(ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer)
{
	if (ev.type == ALLEGRO_EVENT_TIMER)
		if (ev.timer.source == timer)
		{
			if (!dir)
				posX -= moveSpeed;
			else
				posX += moveSpeed;
		}
}

void ENEMY_PROJECTILE::Collision(PLAYER *player)
{
	if (bbox->Intersects(player->bbox))
	{
		player->subCurrentHp(dmg);
		isAlive = false;
	}
}