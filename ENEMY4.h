#pragma once
#include "ENEMY.h"
class ENEMY4 :
	public ENEMY
{
public:
	ENEMY4(float posX, float posY, bool isOnPlatform);
	~ENEMY4();
private:
	void SpawnProjectile(ALLEGRO_EVENT ev, PLAYER* player, ALLEGRO_TIMER *secTimer, ALLEGRO_TIMER* timer);
};

