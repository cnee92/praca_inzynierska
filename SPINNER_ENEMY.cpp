#include "SPINNER_ENEMY.h"

SPINNER_ENEMY::SPINNER_ENEMY(float posX, float posY)
{
	image = al_load_bitmap("Img/spinner.png");
	this->width = al_get_bitmap_width(image);
	this->height = al_get_bitmap_height(image);

	this->posX = posX;
	this->posY = SCREEN_HEIGHT - 160 - height/2;

	this->hpBorder = 100;
	this->maxHp = 50;
	this->currentHp = maxHp;
	this->hpBar = 100;

	this->exp = 25;

	bbox = new BOUNDINGBOX(posX-width/2, posY-height/2, width, height);

	moveSpeed = 12;
	
	isActive = isAlive = true;
	deathExplosion = false;

	imageSourceX = 0;
	dir = true;
	degree = 0;
}


SPINNER_ENEMY::~SPINNER_ENEMY()
{
}

void SPINNER_ENEMY::Update(PLAYER *player, float camerPosX, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER *animationTimer, std::vector<PROJECTILE*> &playerProjectiles, vector<OBSTACKLE*> &obstackles)
{
	if (currentHp <= 0)
		isAlive = false;

	if (isAlive)
	{
		Move(camerPosX, ev, timer, animationTimer);
		delete bbox;
		bbox = new BOUNDINGBOX(posX - width/2, posY - height/2+20, width, height);
		Collision(player, playerProjectiles, obstackles);
	}



	if (!isAlive && !deathExplosion)
	{
		image = al_load_bitmap("Img/wybuch.png");
		width = al_get_bitmap_width(image) / 17;
		height = al_get_bitmap_height(image);
		imageSourceX = 0;
		imageSourceY = 0;
		deathExplosion = true;
	}
}

void SPINNER_ENEMY::Draw()
{
	//bbox->Draw();
	
	if (isAlive)
	{
		DrawHpBar();
		al_draw_rotated_bitmap(image, width / 2, height / 2, posX, posY, degree * 3.14159 / 180, NULL);
	}
	else
	{
		al_draw_bitmap_region(image, imageSourceX, imageSourceY, width, height, posX - width / 2, posY - height / 2, NULL);
		imageSourceX += width;
		if (imageSourceX >= al_get_bitmap_width(image))
			isActive = false;
	}
}


void SPINNER_ENEMY::DrawHpBar()
{
	hpBar = hpBorder * (currentHp / maxHp);

	float hpBarPosX = posX - width / 2;
	float hpBarPosY = posY - height / 2;
	al_draw_filled_rectangle(hpBarPosX, hpBarPosY, hpBarPosX + hpBar, hpBarPosY - 10, al_map_rgb(255, 0, 0));
	al_draw_rectangle(hpBarPosX, hpBarPosY, hpBarPosX + hpBorder, hpBarPosY - 10, al_map_rgb(0, 0, 0), 3);
}



void SPINNER_ENEMY::Move(float camerPosX, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *animationTimer)
{
	if (ev.type == ALLEGRO_EVENT_TIMER)
		if (ev.timer.source == timer)
		{
			if (posX < camerPosX - 100)
				dir = false;
			if (posX + width > camerPosX + SCREEN_WIDTH + 200)
				dir = true;

			if (dir)
			{
				posX -= moveSpeed;
				degree -= 10;
				if (degree <= 0)
					degree = 360;
			}
			else
			{
				posX += moveSpeed;
				degree += 10;
				if (degree >= 360)
					degree = 0;
			}

			imageSourceX += width;
			if (imageSourceX >= al_get_bitmap_width(image) - 10)
				imageSourceX = 0;

			if (dir)
				imageSourceY = 0;
			else
				imageSourceY = 1;
		}
}

void SPINNER_ENEMY::Collision(PLAYER* player, vector<PROJECTILE*> &playerProjectiles, vector<OBSTACKLE*> &obstackles)
{

	if (bbox->Intersects(player->bbox))
	{
		isAlive = false;
		player->subCurrentHp(player->getCurrentHp());
	}

	for (int i = 0; i < playerProjectiles.size(); i++)
		if (bbox->Intersects(playerProjectiles[i]->bbox))
		{
			if (playerProjectiles[i]->getIsAlive())
				currentHp -= playerProjectiles[i]->dmg;

			playerProjectiles[i]->setIsAlive(false);
		}
	
	for (int i = 0; i < obstackles.size(); i++)
	if (bbox->Intersects(obstackles[i]->bbox))
	{
		if (bbox->right >= obstackles[i]->bbox->left && bbox->right < obstackles[i]->bbox->right)
			dir = true;
		else if (bbox->left <= obstackles[i]->bbox->right && bbox->left > obstackles[i]->bbox->left)
			dir = false;
	}

}