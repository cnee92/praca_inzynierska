#include "ENEMY2.h"

ENEMY2::ENEMY2(float posX, float posY, bool isOnPlatform)
{
	float imageFrameX = 7;
	float imageFrameY = 2;

	image = al_load_bitmap("Img/Cov2.png");
	this->width = al_get_bitmap_width(image) / imageFrameX;
	this->height = al_get_bitmap_height(image) / imageFrameY;


	this->posX = this->prevPosX = this->startPosX = (rand() % 280) + posX;
	this->posY = posY - height;
	this->bboxSizeX = 40;

	this->exp = 30;
	this->hpBorder = 100;
	this->maxHp = 130;
	this->currentHp = maxHp;
	this->hpBar = 100;
//	this->hpBarPart = 100 / maxHp;

	bbox = new BOUNDINGBOX(posX + bboxSizeX, posY, width - bboxSizeX, height);

	this->projectleTime = 0;
	moveSpeed = 5;
	isActive = isAlive = true;
	deathExplosion = false;

	this->isOnPlatform = isOnPlatform;
	this->limitPosX = posX + 10;

	if ((rand() % 10) < 5)
		onPlatformDir = false;
	else
		onPlatformDir = true;


}

ENEMY2::~ENEMY2()
{
}


void ENEMY2::SpawnProjectile(ALLEGRO_EVENT ev, PLAYER* player, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER* timer)
{
	if (ev.type == ALLEGRO_EVENT_TIMER)
		if (ev.timer.source == timer6fps)
		{

			if (!isOnPlatform)
			{
				if (projectleTime < 7)
					projectiles.push_back(new ENEMY_PROJECTILE(player->posX, posX, posY-5, width, height));
			}
			else
				if (projectleTime < 7)
					projectiles.push_back(new ENEMY_PROJECTILE(onPlatformDir, posX, posY, width, height));
					

			projectleTime++;

			if (projectleTime >= 15)
				projectleTime = 0;
		}

	for (int i = 0; i < projectiles.size(); i++)
	{
		projectiles[i]->Update(player, ev, timer);

		if (projectiles[i]->getPosX() > projectiles[i]->getStartPosX() + SCREEN_WIDTH / 1.5)
			projectiles[i]->setIsActive(false);
		else if (projectiles[i]->getPosX() < projectiles[i]->getStartPosX() - SCREEN_WIDTH / 1.5)
			projectiles[i]->setIsActive(false);

		if (!projectiles[i]->getIsActive())
		{
			delete projectiles[i];
			projectiles.erase(projectiles.begin() + i);
		}
	}
}