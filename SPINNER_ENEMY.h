#pragma once
#include "ENEMY.h"
#include "OBSTACKLE.h"

class SPINNER_ENEMY :
	public ENEMY
{
public:
	SPINNER_ENEMY(float posX, float posY);
	~SPINNER_ENEMY();

	void Update(PLAYER *player, float camerPosX, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *secTimer, ALLEGRO_TIMER *animationTimer, vector<PROJECTILE*> &playerProjectiles, vector<OBSTACKLE*> &obstackles);
	void Draw();
private:
	bool dir;
	float degree;
	
	void Move(float camerPosX, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *animationTimer);
	void DrawHpBar();
	void Collision(PLAYER* player, std::vector<PROJECTILE*> &playerProjectiles, vector<OBSTACKLE*> &obstackles);
};

