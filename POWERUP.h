#pragma once
#include <allegro5\allegro.h>

#include "INPUT_MANAGER.h"
#include "PLAYER.h"

class POWERUP
{
public:
	POWERUP();
	~POWERUP();

	void Update(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer1fps);
	void Draw(float cameraPosX);
private:
	INPUT_MANAGER input;
	ALLEGRO_FONT *font;
	ALLEGRO_FONT *font1;



	ALLEGRO_BITMAP *healingButton;
	//ALLEGRO_BITMAP *healingImage;
	string healingTimeText;

	float healingPosX;
	float healingPosY;
	int healingButtonWidth;
	int healingButtonHeight;
	//int healingImageWidth;
	//int healingImageHeight;

	bool healingAvailable;
	bool healingReady;
	bool healingStart;
	float healLimit;
	float healedPoint;
	int healingCD;




	ALLEGRO_BITMAP *ddButton;
	string ddTimeText;

	float ddPosX;
	float ddPosY;
	int ddButtonWidth;
	int ddButtonHeight;

	bool ddAvailable;
	bool ddReady;
	int ddCD;
	int ddDuration;

	BOUNDINGBOX *shieldBBox;
	ALLEGRO_BITMAP *shieldButton;
	ALLEGRO_BITMAP *shieldImage;
	string shieldTimeText;

	float shieldImgPosX;
	float shieldImgPosY;
	float shieldPosX;
	float shieldPosY;
	int shieldButtonWidth;
	int shieldButtonHeight;

	bool shieldAvailable;
	bool shieldReady;
	bool shieldActive;
	int shieldCD;
	int shieldDuration;
	
	void CheckInput(PLAYER *player, ALLEGRO_EVENT ev);

	void Healing(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer1fps);
	void DrawHeling(float cameraPosX);

	void DoubleDmg(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer1fps);
	void DrawDoubleDmg(float cameraPosX);

	void Shield(PLAYER *player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer1fps);
	void DrawShield(float cameraPosX);
	

};
