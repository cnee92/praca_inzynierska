#pragma once

#include <vector>
#include<allegro5\allegro.h>
#include<allegro5\allegro_primitives.h>

#include "DEFINITIONS.h"
#include "BOUNDINGBOX.h"
#include "PROJECTILE.h"
#include "ENEMY_PROJECTILE.h"
#include "POWERUP.h"

class ENEMY : public PLAYER
{
public:
	ENEMY();
	ENEMY(float posX, float posY, bool isOnPlatform);
	~ENEMY();

	virtual void Update(PLAYER* player, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER *animationTimer, std::vector<PROJECTILE*> &playerProjectiles);
	virtual void Draw();

	virtual bool getIsActive();
	virtual void setIsActive(bool isActive);
	virtual vector<ENEMY_PROJECTILE*> getEnemiesProjectiles();
	virtual int getExp();
	BOUNDINGBOX *bbox;

	std::vector<ENEMY_PROJECTILE*> projectiles;

protected:

	ALLEGRO_BITMAP *image;


	float startPosX;
	int projectleTime;

	bool isOnPlatform;
	float limitPosX;
	bool onPlatformDir;

	float newHeight;

	int exp;

	bool isActive;
	bool isAlive;
	bool deathExplosion;


	virtual void Move(float playerPosX, ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *animationTimer);
	virtual void Collision(std::vector<PROJECTILE*> &playerProjectiles);
	virtual void DrawHpBar();
	virtual void SpawnProjectile(ALLEGRO_EVENT ev, PLAYER* player, ALLEGRO_TIMER *timer6fps, ALLEGRO_TIMER* timer);
};

