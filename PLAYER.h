#pragma once
#include <iostream>
#include <string>
#include<allegro5\allegro.h>
#include<allegro5\allegro_primitives.h>
#include<allegro5\allegro_image.h>
#include<allegro5\allegro_ttf.h>
#include<allegro5\allegro_font.h>

#include "DEFINITIONS.h"
#include "BOUNDINGBOX.h"
#include "INPUT_MANAGER.h"

class PLAYER
{
public:
	PLAYER();
	PLAYER(ALLEGRO_DISPLAY *display);
	~PLAYER();

	void Update(ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *animationTimer, bool &gameOver);
	void Draw(float cameraPosX);

	virtual float getWidth();
	virtual float getHeight();
	bool getOnSolid();
	void setOnSolid(bool onSolid);
	virtual float getBboxSizeX();
	virtual float getCurrentHp();
	void subCurrentHp(float value);
	void addExperience(int exp);
	float getMaxHP();
	int getDmg();
	void setDmg(int dmg);
	bool getIsDDActive();
	void setIsDDActive(bool isDDActive);
	void LoadPlayerImg(const char* fileName);
	bool getSquat();
	bool getIsShieldActive();
	void setIsShieldActive(bool isShieldActive);
	float getLvl();


	float posX;
	float posY;
	float prevPosX;
	float prevPosY;

	float redraw;
	BOUNDINGBOX *bbox, *prevBbox;
	float bboxSizeX;
	float bboxSizeY;

	float projectilePosY;

	float velY;

	int projectileDirRight;
	int projectileDirLeft;

protected:
	INPUT_MANAGER input;
	//ANIMATION
	bool animationActive;
	float imageSourceX, imageSourceY, squatImageSourceY;

	float lvl;
	float experience;
	float expToLvl;
	float lvlBar;
	string expText;
	bool lvlUp;
	ALLEGRO_BITMAP *lvlUpImg;
	float lvlUpPosX;
	float lvlUpPosY;
	float lvlUpWidth;
	float lvlUpHeight;

	float scale;

	float maxHp;
	float hpBar;
	float currentHp;
	float hpBorder;
	string hpText;


	ALLEGRO_BITMAP *image;
	ALLEGRO_BITMAP *hpBorderImg;
	ALLEGRO_FONT *font;

	


	enum direction {RIGHT, LEFT, JUMP, SQUAT };
	bool dirKeys[4] = { false, false, false, false };
	
	int width;
	int height;

	int dmg;
	bool isDDActive;
	bool isShieldActive;

	int moveSpeed;
	bool jump;
	bool jumpKeyPressed;
	bool onSolid;
	float jumpSpeed;
	float groundY;
	bool squat;
	
	bool isActive;
	bool isAlive;
	bool deathExplosion;
	float newHeight;

	void Move(ALLEGRO_EVENT ev, ALLEGRO_TIMER *timer, ALLEGRO_TIMER *animationTimer);
	void CheckInput(ALLEGRO_EVENT ev);
	void Jump();
	void DrawBars(float posX);
	void LvlUP();
};

