#include "OBSTACKLE.h"



OBSTACKLE::OBSTACKLE(float posX)
{
	image = al_load_bitmap("Img/obstackle.png");
	width = al_get_bitmap_width(image);
	height = al_get_bitmap_height(image);
	this->posX = posX;
	posY = SCREEN_HEIGHT - 150 - height;

	bbox = new BOUNDINGBOX(posX, posY, width, height);
}

void OBSTACKLE::Update(PLAYER *player, vector<PROJECTILE*> &playerProjectile, ENEMY *groundEnemy[10])
{
	Collision(player, playerProjectile, groundEnemy);
}

void OBSTACKLE::Draw()
{
	//bbox->Draw();
	al_draw_bitmap(image, posX, posY, NULL);
}

OBSTACKLE::~OBSTACKLE()
{
}

void OBSTACKLE::Collision(PLAYER *player, vector<PROJECTILE*> &playerProjectile, ENEMY *groundEnemy[10])
{
	if (bbox->Intersects(player->bbox))
	{
		if (player->bbox->bottom >= bbox->top && player->prevBbox->bottom <= bbox->top)
		{
			player->posY = posY - player->getHeight();
			player->subCurrentHp(player->getCurrentHp());
		}
		else if (player->bbox->right >= bbox->left && player->prevBbox->right <= bbox->left)
			player->posX = (bbox->left - player->getWidth() + player->getBboxSizeX());

		else if (player->bbox->left <= bbox->right && player->prevBbox->left >= bbox->right)
			player->posX = (bbox->left + width - player->getBboxSizeX());
	}

	for (int i=0; i < playerProjectile.size(); i++)
		if (bbox->Intersects(playerProjectile[i]->bbox))
			playerProjectile[i]->setIsActive(false);

	for (int i = 0; i < 10; i++)
		if (groundEnemy[i] != NULL)
		{
			if (bbox->Intersects(groundEnemy[i]->bbox))
			{
				if (groundEnemy[i]->bbox->right >= bbox->left && groundEnemy[i]->prevBbox->right <= bbox->left)
					groundEnemy[i]->posX = (bbox->left - groundEnemy[i]->getWidth() + groundEnemy[i]->getBboxSizeX());

				if (groundEnemy[i]->bbox->left <= bbox->right && groundEnemy[i]->prevBbox->left >= bbox->right)
					groundEnemy[i]->posX = (bbox->left + width - groundEnemy[i]->getBboxSizeX());
			}

			for (int j = 0; j < groundEnemy[i]->projectiles.size(); j++)
				if(groundEnemy[i]->projectiles[j]->bbox!=NULL)
					if (bbox->Intersects(groundEnemy[i]->projectiles[j]->bbox))
						groundEnemy[i]->projectiles[j]->setIsActive(false);
		}
}
