#pragma once
#include<allegro5\allegro.h>

#include"BOUNDINGBOX.h"
#include "PLAYER.h"
#include "PROJECTILE.h"

class BOSS
{
public:
	BOSS();
	~BOSS();

	bool isActive;

	void Update(ALLEGRO_EVENT ev, ALLEGRO_TIMER *animationTimer, PLAYER *player, std::vector<PROJECTILE*> &playerProjectiles, bool &bossDied, bool &bossIsActive);
	void Draw(float cameraPosX);
private:

	ALLEGRO_FONT *font;
	string hpText;
	float hpBorderSize;
	float hpBar;
	float maxHp;
	float currentHp;
	float hpBarPosX;
	float hpBarPosY;
	ALLEGRO_BITMAP *hpBorderImage;
	BOUNDINGBOX *eyeBBox;

	float opacity;

	ALLEGRO_BITMAP *footImage;
	ALLEGRO_BITMAP *headImage;
	float headPosX;
	float headPosY;
	float headStartPosX;
	float headStartPosY;
	float headMoveX;
	float headMoveY;
	bool headActive;
	bool headFlag;


	ALLEGRO_BITMAP *jawImage;
	float jawPosX;
	float jawPosY;
	float jawDegree;
	bool jawActive;
	bool jawDown;
	bool jawUp;


	BOUNDINGBOX *bigAttackBBox;
	ALLEGRO_BITMAP* bigAttackImage;
	float bigAttackPosX;
	float bigAttackPosY;
	float bigAttackWidth;
	float bigAttackHeight;
	float bigAttackScale;
	bool bigAttackActive;

	ALLEGRO_BITMAP* fullBigAttackImage;
	float fullBigAttackPosX;
	float fullBigAttackPosY;
	float fullBigAttackWidth;
	float fullBigAttackHeight;


	BOUNDINGBOX *smallAttackBBox;
	ALLEGRO_BITMAP* smallAttackImage;
	float smallAttackPosX;
	float smallAttackPosY;
	float smallAttackWidth;
	float smallAttackHeight;
	float smallAttackScale;
	bool smallAttackActive;

	ALLEGRO_BITMAP* fullSmallAttackImage;
	float fullSmallAttackPosX;
	float fullSmallAttackPosY;
	float fullSmallAttackWidth;
	float fullSmallAttackHeight;


	void JawMove(ALLEGRO_EVENT ev, ALLEGRO_TIMER *animationTimer, PLAYER* player);
	void HeadMove(ALLEGRO_EVENT ev, ALLEGRO_TIMER *animationTimer);
	void SetAttackPositions();
	void Collision(PLAYER *player, std::vector<PROJECTILE*> &playerProjectiles);
	void DrawHpBar(float posX);
	void DrawAttacks();
	

};

